const mongoose = require('mongoose')

var Schema = mongoose.Schema;

const ReviewSchema = new mongoose.Schema({
    album_id : {
      type : Schema.Types.ObjectId
    },
    stars : {
      type : Number
    },
    posted: {
      type: Date,
      required: true
    },
    body : {
        type : String,
        required : true
    }
  })

const UserSchema = new mongoose.Schema({
    nick : {
        type: String,
        required : true
    },
    registered : {
        type: Date,
        required: true
    },
    reviews : [ReviewSchema],
    friends : [{
        type: Schema.Types.ObjectId,
        ref : 'User'
    }]

})

var User = mongoose.model("User", UserSchema);
module.exports = User;