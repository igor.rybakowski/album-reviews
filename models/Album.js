const mongoose = require('mongoose');
const { Song } = require('..');

var Schema = mongoose.Schema;

const SongSchema = new mongoose.Schema({
    name: String,
    number : {type: Number, unique : true},
    length : Number

})

const AlbumSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    artist_id : {
        type : Schema.Types.ObjectId,
        ref : 'Artist'
    },
    genre: {
        type: String,
        required: true
    },
    release_year: {
        type: Number,
        required: false
    },
    songs : [SongSchema],
    reviews: [{
        type: Schema.Types.ObjectId,
        ref : 'Review'
    }],
    likes : {
        type: Number
    }
  })

var Album = mongoose.model("Album", AlbumSchema);
module.exports = Album;