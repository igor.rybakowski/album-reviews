const mongoose = require('mongoose')

var Schema = mongoose.Schema;

const ArtistSchema = new mongoose.Schema({
    name: {
      type: String,
      required: true
    },
    start_year: {
      type: Number,
      required: false
    },
    albums : [{
        type: Schema.Types.ObjectId,
        ref : 'Album'
    }]
  })

var Artist = mongoose.model("Artist", ArtistSchema);
module.exports = Artist;