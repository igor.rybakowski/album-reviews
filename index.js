module.exports = {
    Album : require("./models/Album"),
    Artist : require("./models/Artist"),
    User : require("./models/User")
}

const {runServer} = require("./server");

runServer(3000);