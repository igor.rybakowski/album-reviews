var express = require("express");
var mongoose = require("mongoose");
const songs = require('./routes/songs')
const albums = require('./routes/albums')
const artists = require('./routes/artists')
const users = require('./routes/users')
const reviews = require('./routes/reviews')

// Require all models
var db = require(".")
console.log(db);

// Connect to MongoDB
mongoose.connect("mongodb://localhost/new", { useNewUrlParser: true });

const port = 3000;

// Initialize Express
var app = express();

// Parse request body as JSON
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Make public static folder
app.use(express.static("public"));

// Routes
app.use('/songs', songs);
app.use('/albums', albums);
app.use('/users', users);
app.use('/artists', artists);
app.use('/reviews', reviews)

// Home route. Currently just to make sure app is running returns hello message.
app.get("/", function(req, res) {
  res.send("Hello from demo app!");
});

const runServer = (port) => {
    app.listen(port, () => {
      console.log('REST server is listening at http://localhost:${port}');
    });
}

module.exports = {
    runServer,
    app
}