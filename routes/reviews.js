const express = require('express')
const { isValidObjectId } = require('mongoose');
const router = express.Router()
var db = require("..")

router.get('/', (req, res) => {
  db.User.aggregate(
    [
        {
            "$unwind" : "$reviews"
        },
        {
          "$group" : {
            "_id" : "$reviews._id",
            "author" : {$first : "$nick"},
            "stars" : {$first : "$reviews.stars"},
            "album" : {$first : "$reviews.album_id"},
            "body" : {$first : "$reviews.body"}
          }
        }
    ]).exec((err, reviews) => {
      if (err) {
        res.json(err.message)
      } else { res.json(reviews) }
})});



// Get song by ID
router.get('/:id', (req, res) => {
  const id = req.params.id;
  db.User.findOne({'reviews._id' : id}, {'reviews.$' : 1} ,function(error, dbSong) {
    if(dbSong) {
      res.json(dbSong);
    }
    else {
      return res.status(404).json({
        message: 'Review with id ' + id + ' can not be found.'
      });
    }})
})


module.exports = router