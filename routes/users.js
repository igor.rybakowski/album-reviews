const express = require('express')
var mongoose = require("mongoose");
const router = express.Router()
var db = require("..")

// Get all songs
router.get('/', (req, res) => {
    db.User.find({})
    .then(function(dbSong) {
      res.json(dbSong);
    })
    .catch(function(err) {
      res.json(err);
    })
});

// Search for Users
router.get("/search", function(req, res) {
  db.User.find(req.query, function (err,dbUser) {
    if(dbUser) {
      res.json(dbUser);
    }
    else {
      return res.status(404).json({
        message : 'Could not find anything'
      });
    }
  })
 })

//Get User by ID
router.get('/:id', (req, res) => {
  const id = req.params.id;
  db.User.findById(id, function(error, dbUser) {
    // Handle the error using the Express error middleware
    // Render not found error
    if(dbUser) {
      res.json(dbUser);
    }
    else {
      return res.status(404).json({
        message: 'User with id ' + id + ' can not be found.'
      });
    }})
})

router.get("/match", (req, res) => {
  const key = req.params.match;
  db.User.aggregate([
    {
       $match : { genre : key}
    }]).then(function(dbUser) {
      res.json(dbUser);
    })
    .catch(function(err) {
      res.json(err);
    })
})

// Add User
router.post('/add', (req, res) => {
    db.User.create(req.body)
    .then(function(dbUser) {
        req.body._id;
        // If we were able to successfully create a Product, send it back to the client
        res.json(dbUser);
    })
    .catch(function(err) {
        // If an error occurred, send it to the client
        res.json(err);
    });
})

// Update User
router.put('/:id', function(req, res, next) {
    var id = req.params.id,
         body = req.body;
    
    db.User.findById(id, function(error, dbUser) {
      // Handle the error using the Express error middleware
      if(error) return next(error);
      
      // Render not found error
      if(!dbUser) {
        return res.status(404).json({
          message: 'User with id ' + id + ' can not be found.'
        });
      }
      
      // Update the course model
      dbUser.update(body, function(error, dbUser) {
        if(error) return next(error);
        
        res.json(dbUser);
      });
    });
  });

// Delete User
router.delete('/:id', (req, res) => {
  var id = req.params.id;
  db.User.findByIdAndDelete(id, function(err, dbUser){
    if(err) {
      return res.status(404).json({
        message : 'User with id ' + id + ' can not be found.'
      });
    }
    else {
      return res.json({
        message : 'User with id ' + id + ' deleted.'
      });
    }
  })

})

module.exports = router