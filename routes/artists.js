const express = require('express')
const router = express.Router()
var db = require("..")

// Get all songs
router.get('/', (req, res) => {
    db.Artist.find({})
    .then(function(dbSong) {
      res.json(dbSong);
    })
    .catch(function(err) {
      res.json(err);
    })
});

router.get("/search", function(req, res) {
  db.Artist.find(req.query, function (err,dbArtist) {
    if(dbArtist) {
      res.json(dbArtist);
    }
    else {
      return res.status(404).json({
        message : 'Could not find anything'
      });
    }
  })
 })

// Get artist by ID
router.get('/:id', (req, res) => {
  const id = req.params.id;
  db.Artist.findById(id, function(error, dbArtist) {
    // Handle the error using the Express error middleware
    // Render not found error
    if(dbArtist) {
      res.json(dbArtist);
    }
    else {
      return res.status(404).json({
        message: 'Artist with id ' + id + ' can not be found.'
      });
    }})
})
// Add Artist
router.post('/add', (req, res) => {
    db.Artist.create(req.body)
    .then(function(dbSong) {
        // If we were able to successfully create a Product, send it back to the client
        res.json(dbSong);
    })
    .catch(function(err) {
        // If an error occurred, send it to the client
        res.json(err);
    });
})

// Update Artist
router.put('/:id', function(req, res, next) {
  var id = req.params.id,
       body = req.body;
  
  db.Artist.findById(id, function(error, dbArtist) {
    // Handle the error using the Express error middleware
    if(error) return next(error);
    
    // Render not found error
    if(!dbArtist) {
      return res.status(404).json({
        message: 'Artist with id ' + id + ' can not be found.'
      });
    }
    
    // Update the course model
    dbArtist.update(body, function(error, dbArtist) {
      if(error) return next(error);
      
      res.json(dbArtist);
    });
  });
});

// Delete Artist
router.delete('/:id', (req, res) => {
var id = req.params.id;
db.Artist.findByIdAndDelete(id, function(err, dbArtist){
  if(err) {
    return res.status(404).json({
      message : 'Artist with id ' + id + ' can not be found.'
    });
  }
  else {
    return res.json({
      message : 'Artist with id ' + id + ' deleted.'
    });
  }
})

})

module.exports = router