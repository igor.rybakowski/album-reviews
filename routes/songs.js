const express = require('express');
const { isValidObjectId } = require('mongoose');
const router = express.Router()
var db = require("..")


router.get('/', (req, res) => {
  db.Album.aggregate(
    [
        {
            "$unwind" : "$songs"
        },
        {
          "$group" : req.body
            // "_id" : "$songs._id",
            // "name" : {$first : "$songs.name"},
            // "album" : {$first : "$name"},
            // "artist" : {$first : "$artist_id"},
            // "length" : {$first : "$songs.length"}
          
        }
    ]).exec((err, songs) => {
      if (err) {
        res.json(err.message)
      } else { res.json(songs) }
})});



// Get song by ID
router.get('/:id', (req, res) => {
  const id = req.params.id;
  db.Album.findOne({'songs._id' : id}, {'songs.$' : 1} ,function(error, dbSong) {
    if(dbSong) {
      res.json(dbSong);
    }
    else {
      return res.status(404).json({
        message: 'Song with id ' + id + ' can not be found.'
      });
    }})
})


// // Update Song
// router.put('/:id', function(req, res, next) {
//   var id = req.params.id,
//        body = req.body;
  
//   db.Song.findById(id, function(error, dbSong) {
//     // Handle the error using the Express error middleware
//     if(error) return next(error);
    
//     // Render not found error
//     if(!dbSong) {
//       return res.status(404).json({
//         message: 'Song with id ' + id + ' can not be found.'
//       });
//     }
    
//     // Update the course model
//     dbSong.update(body, function(error, dbSong) {
//       if(error) return next(error);
      
//       res.json(dbSong);
//     });
//   });
// });

// Search for songs
// router.get("/search", function(req, res) {
//   db.Album.find({'songs.$' : req.query}, function (err,dbSong) {
//     if(dbSong) {
//       res.json(dbSong);
//     }
//     else {
//       return res.status(404).json({
//         message : 'Could not find anything'
//       });
//     }
//   })
//  })

// Delete Song
// router.delete('/:id', (req, res) => {
// var id = req.params.id;
// db.Song.findByIdAndDelete(id, function(err, dbSong){
//   if(err) {
//     return res.status(404).json({
//       message : 'Song with id ' + id + ' can not be found.'
//     });
//   }
//   else {
//     return res.json({
//       message : 'Song with id ' + id + ' deleted.'
//     });
//   }
// })

// })
module.exports = router