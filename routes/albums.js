const express = require('express')
var mongoose = require("mongoose");
const router = express.Router()
var db = require("..")

// Get all songs
router.get('/', (req, res) => {
    db.Album.find({})
    .then(function(dbSong) {
      res.json(dbSong);
    })
    .catch(function(err) {
      res.json(err);
    })
});

// Search for albums
router.get("/search", function(req, res) {
  db.Album.find(req.query, function (err,dbAlbum) {
    if(dbAlbum) {
      res.json(dbAlbum);
    }
    else {
      return res.status(404).json({
        message : 'Could not find anything'
      });
    }
  })
 })

//Get Album by ID
router.get('/:id', (req, res) => {
  const id = req.params.id;
  db.Album.findById(id, function(error, dbAlbum) {
    // Handle the error using the Express error middleware
    // Render not found error
    if(dbAlbum) {
      res.json(dbAlbum);
    }
    else {
      return res.status(404).json({
        message: 'Album with id ' + id + ' can not be found.'
      });
    }})
})

router.get("/match", (req, res) => {
  const key = req.params.match;
  db.Album.aggregate([
    {
       $match : { genre : key}
    }]).then(function(dbAlbum) {
      res.json(dbAlbum);
    })
    .catch(function(err) {
      res.json(err);
    })
})

// Add Album
router.post('/add', (req, res) => {
    db.Album.create(req.body)
    .then(function(dbAlbum) {
        req.body._id;
        // If we were able to successfully create a Product, send it back to the client
        res.json(dbAlbum);
    })
    .catch(function(err) {
        // If an error occurred, send it to the client
        res.json(err);
    });
})

// Update Album
router.put('/:id', function(req, res, next) {
    var id = req.params.id,
         body = req.body;
    
    db.Album.findById(id, function(error, dbAlbum) {
      // Handle the error using the Express error middleware
      if(error) return next(error);
      
      // Render not found error
      if(!dbAlbum) {
        return res.status(404).json({
          message: 'Album with id ' + id + ' can not be found.'
        });
      }
      
      // Update the course model
      dbAlbum.update(body, function(error, dbAlbum) {
        if(error) return next(error);
        
        res.json(dbAlbum);
      });
    });
  });

// Delete Album
router.delete('/:id', (req, res) => {
  var id = req.params.id;
  db.Album.findByIdAndDelete(id, function(err, dbAlbum){
    if(err) {
      return res.status(404).json({
        message : 'Album with id ' + id + ' can not be found.'
      });
    }
    else {
      return res.json({
        message : 'Album with id ' + id + ' deleted.'
      });
    }
  })

})

module.exports = router